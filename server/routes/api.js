const express = require('express');
const router = express.Router();

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/angular-test');

var Source = require('../../src/app/models/source.js');

/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});


/******** Sources *********/
router.route('/sources')
  .get(function(req,res) {
    // get all the sources
    Source.find(function(err, sources) {
      if (err) res.status(500).send(err);
      //object of all the sources
      res.json(sources);
    });
  })
  .post(function(req, res) {
    // Create new source
    var newSource = Source({
      title: req.body.title,
      type: req.body.type,
      meta: {
        duration: req.body.duration,
        url: req.body.url
      },
      created_at: Date.now(),
      updated_at: Date.now()
    });
    // save the source
    newSource.save(function(err, createdSource) {
      if (err) res.status(500).send(err);
      res.send(createdSource);
    })
  });

  router.route('/sources/:source_id')
  .delete(function(req, res) {
        Source.remove({
            _id: req.params.source_id
        }, function(err, source) {
            if (err) res.status(500).send(err);
            res.send({ message: 'Successfully deleted' });
        });
    });


module.exports = router;
