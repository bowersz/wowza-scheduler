import { Component, OnInit } from '@angular/core';
import { SourceComponent } from './sources.component';
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'Wowza Scheduler';

  ngOnInit(): void {
    $('.modal').modal();
  }

}
