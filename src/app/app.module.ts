import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule }   from '@angular/router';

import { SourceComponent} from './sources.component';
import { AppComponent } from './app.component';
import { ScheduleHomeComponent } from './schedule-home.component';
import { CalendarComponent } from "angular2-fullcalendar/src/calendar/calendar";
import { SchedulerComponent } from './scheduler.component';
import { DataService } from './services/data.service';

@NgModule({
  declarations: [
    AppComponent,
    SourceComponent,
    ScheduleHomeComponent,
    SchedulerComponent,
    CalendarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
