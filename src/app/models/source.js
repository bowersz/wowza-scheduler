// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/*export class Source {
  constructor(){}
  title: string;
  type: string;
  meta: {
    duration: number,
    url: string
  }
  createdDate: Date;
  updatedDate: Date;
}*/

// create a schema
var sourceSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  meta: {
    duration: {
      type: Number,
      required: true
    },
    url: {
      type: String,
      required: true
    }
  },
  created_at: Date,
  updated_at: Date
});

// the schema is useless so far
// we need to create a model using it
var Source = mongoose.model('Source', sourceSchema);

module.exports = Source;
