import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'scheduler',
  templateUrl: './scheduler.component.html'
})

export class SchedulerComponent {

  calendarOptions:Object = {
    height: 700,
    defaultView: 'agendaWeek',
    fixedWeekCount : false,
    defaultDate: '2017-2-12',
    editable: true,
    eventStartEditable: true,
    eventDurationEditable: true,
    eventOverlap: false,
    selectable: true,
    selectHelper: true,
    slotDuration: '00:15:00',
    eventLimit: true, // allow "more" link when too many events
    events: [
      {
        title: 'All Day Event',
        start: '2017-2-11',
        end: '2017-2-11'
      },
      {
        title: 'Long Event',
        start: '2017-2-12',
        end: '2017-2-12'
      },
      {
        id: 999,
        title: 'Repeating Event',
        start: '2016-09-09T16:00:00'
      },
      {
        id: 999,
        title: 'Repeating Event',
        start: '2016-09-16T16:00:00'
      },
      {
        title: 'Conference',
        start: '2016-09-11',
        end: '2016-09-13'
      },
      {
        title: 'Meeting',
        start: '2016-09-12T10:30:00',
        end: '2016-09-12T12:30:00'
      },
      {
        title: 'Lunch',
        start: '2016-09-12T12:00:00'
      },
      {
        title: 'Meeting',
        start: '2016-09-12T14:30:00'
      },
      {
        title: 'Happy Hour',
        start: '2017-02-12T17:30:00'
      },
      {
        title: 'Dinner',
        start: '2016-09-12T20:00:00'
      },
      {
        title: 'Birthday Party',
        start: '2016-09-13T07:00:00'
      },
      {
        title: 'Click for Google',
        url: 'http://google.com/',
        start: '2016-09-28'
      }
    ],
    eventDrop: function(event, delta, revertFunc) {

        alert(event.title + " was dropped on " + event.start.format());
        alert(event);
        alert(delta);

        if (!confirm("Are you sure about this change?")) {
            revertFunc();
        }

    },
    select: function( start, end, jsEvent, view ) {
        alert("time selected from:"+start+"through"+end);
    }
  };


}
