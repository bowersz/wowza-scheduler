import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class DataService {

  constructor(private http: Http) { }

  private headers = new Headers({'Content-Type': 'application/json'});

    // Get all posts from the API
    getAllSources() {
      console.log("Getting Sources");
      return this.http.get('api/sources')
      .toPromise()
      .catch(this.handleError);
    }

    // Create a new source
    createNewSource(title: string, type: string, duration: number, URL: string): Promise<any> {
      console.log("Creating Source");
      return this.http.post('api/sources', JSON.stringify({title: title, type: type, duration: duration, url: URL}), {headers: this.headers})
      .toPromise()
      .catch(this.handleError);
    }

    // Delet a source-info
    deleteSource(sourceId: string) {
      console.log("Deleting Source");
      return this.http.delete('api/sources/'+sourceId)
      .toPromise()
      .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
      console.error('An error occurred', error);
      return Promise.reject(error.message || error);
    }

}
