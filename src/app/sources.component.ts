import { Component, OnInit } from '@angular/core';
import { DataService} from './services/data.service';
declare var $: any;
declare var Materialize : any;

@Component({
  selector: 'app-sources',
  templateUrl: './sources.component.html',
  styleUrls: ['./sources.component.css']
})

export class SourceComponent implements OnInit {
  sourceSectionTitle = 'My Sources';
  sources;
  liveSources = [];
  vodSources = [];
  currentSource;

  constructor(private dataService: DataService) {
    }

  ngOnInit(): void {
    console.log("hello there");
    this.emptySource();
    this.getSources();
    $('.modal').modal();
    $('select').material_select();
    $('.collapsible').collapsible();
  }

  createSource() {
    console.log("TYPE IS: " + this.currentSource.type);
    this.dataService.createNewSource(this.currentSource.title,this.currentSource.type,this.currentSource.meta.duration,this.currentSource.meta.url)
    .then((source) => {
      console.log("source is:");
      console.log(source);
      this.emptySource();
      this.getSources();
    },function(reason) {
        console.log("Uh Oh, there has been an eror.");
        Materialize.toast('Error creating source. Please try again.', 4000);
      });
  }
  getSources(){
    this.dataService.getAllSources()
    .then(sources => {
      console.log("Dude, sources: " + sources.json());
      this.sources = sources.json();
      this.liveSources = [];
      this.vodSources = [];
      for (let source of this.sources) {
        console.log("parsing" + source);
        if (source.type == "live") {
          this.liveSources.push(source);
        } else {
          this.vodSources.push(source);
        }
      }

    },function(reason) {

      });
  }
  deleteSource(thisSource){
    var sourceId = thisSource._id;
    this.dataService.deleteSource(sourceId)
    .then(message => {
      $('.modal').modal('close');
      this.getSources();
    },function(reason) {
        console.log("Uh Oh, there has been an eror.");
      });
  }
  emptySource(){
    this.currentSource = {
        title : "",
        type : "",
        _id : "",
        meta: {
          duration : "",
          url : ""
        }
    }
  }

}
